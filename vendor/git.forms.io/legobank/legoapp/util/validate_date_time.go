package util

import (
	"fmt"
	"time"
)

func ValidateDate(date string) error {

	_, err := time.Parse("2006-01-02", date)
	//Date checked failed, the correct format is: YYYY-MM-DD
	if err != nil {
		return fmt.Errorf("date format checked failed, the correct format is: YYYY-MM-DD")
	}
	return nil

}

func ValidateTime(t string) error {

	_, err := time.Parse("15:04:05", t)

	if err != nil {
		return fmt.Errorf("time format checked failed, the correct format is: HH:MM:SS")
	}

	return nil
}

func ValidateDateTime(dateTime string) error {

	_, err := time.Parse("2006-01-02 15:04:05", dateTime)

	if err != nil {
		return fmt.Errorf("datetime format checked failed, the correct format is: YYYY-MM-DD HH:MM:SS")
	}

	return nil
}
