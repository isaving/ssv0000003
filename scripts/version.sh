#!/bin/bash
set -e -x

#if [ -n "$(git status --porcelain --untracked-files=no)" ]; then
#    DIRTY="_dirty"
#fi
#echo "$@"
COMMIT=$(git rev-parse --short HEAD)
GIT_TAG=$(git tag -l --contains HEAD | head -n 1)

if [[ -z "$DIRTY" && -n "$GIT_TAG" ]]; then
    VERSION=$GIT_TAG
else
    VERSION="${COMMIT}${DIRTY}"
fi

echo ${VERSION}_$(echo $(date +%Y%m%d%H)|cut -c 3-10)_$(git symbolic-ref --short -q HEAD)
