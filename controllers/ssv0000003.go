//Version: v0.01
package controllers

import (
	"git.forms.io/isaving/sv/ssv0000003/models"
	"git.forms.io/isaving/sv/ssv0000003/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv0000003Controller struct {
	controllers.CommController
}

func (*Ssv0000003Controller) ControllerName() string {
	return "Ssv0000003Controller"
}

// @Desc ssv0000003 controller
// @Description Entry
// @Param ssv0000003 body models.SSV0000003I true "body for user content"
// @Success 200 {object} models.SSV0000003O
// @router /ssv0000003 [post]
// @Author
// @Date 2020-12-04
func (c *Ssv0000003Controller) Ssv0000003() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv0000003Controller.Ssv0000003 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv0000003I := &models.SSV0000003I{}
	if err := models.UnPackRequest(c.Req.Body, ssv0000003I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv0000003I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv0000003 := &services.Ssv0000003Impl{}
	ssv0000003.New(c.CommController)
	ssv0000003.Sv000003I = ssv0000003I

	ssv0000003O, err := ssv0000003.Ssv0000003(ssv0000003I)

	if err != nil {
		log.Errorf("Ssv0000003Controller.Ssv0000003 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv0000003O.PackResponse()
	if err != nil {
		c.SetServiceError(err)
		return
	}
	c.SetAppBody(responseBody)
}

// Title Ssv0000003 Controller
// Description ssv0000003 controller
// Param Ssv0000003 body models.SSV0000003I true body for SSV0000003 content
// Success 200 {object} models.SSV0000003O
// router /create [post]
func (c *Ssv0000003Controller) SWSsv0000003() {
	//Here is to generate API documentation, no need to implement methods
}
