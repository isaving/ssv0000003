//Version: v0.01
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV0000003I struct {
	QryTpy        string `json:"QryTpy" description:"Query type" validate:"required,max=30"`  //查询类型
	MediaType     string `json:"MediaType" description:"Medium type" validate:"max=4"`        //介质类型
	MediaNm       string `json:"MediaNm" description:"Medium number" validate:"max=40"`       //介质号码
	AgreementId   string `json:"AgreementId" description:"Contract number" validate:"max=30"` //合约号
	AgreementType string `json:"AgreementType" description:"Contract type" validate:"max=5"`  //合约类型
	CstmrId       string `json:"CstmrId" description:"Customer number" validate:"max=20"`     //客户编号
}

type SSV0000003O struct {
	Records []ContInfo `json:"Records" description:"Contract information"` //合约信息

}

type ContInfo struct {
	AgreementType string  `json:"AgreementType" description:"Contract type"`              //合约类型
	AgreementId   string  `json:"AgreementId" description:"Contract number"`              //合约号
	Currency      string  `json:"Currency" description:"Currency"`                        //币种
	CashtranFlag  string  `json:"CashtranFlag" description:"Flag of cash and remittance"` //钞汇标识
	Balance       float64 `json:"Balance" description:"Balance"`                          //余额
	FrzAmount     float64 `json:"FrzAmount" description:"Frozen amount"`                  //冻结金额
	AvlBalance    float64 `json:"AvlBalance" description:"Available balance"`             //可用余额
	AccStatus     string  `json:"AccStatus" description:"Account status"`                 //账户状态
	AccOpnDt      string  `json:"AccOpnDt" description:"Account opening date"`            //开户日期
	CstmrCntctPh  string  `json:"CstmrCntctPh" description:"Mobile phone number"`         //手机号
	CstmrCntctAdd string  `json:"CstmrCntctAdd" description:"Customer contact address"`   //联系地址
	CstmrCntctEm  string  `json:"CstmrCntctEm" description:"Customer email"`              //邮箱
	AccuntNme     string  `json:"AccuntNme" description:"Account name"`                   //账户姓名
	FreezeType    string  `json:"FreezeType" description:"Frozen state"`                  //冻结状态
}

// @Desc Build request message
func (o *SSV0000003I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV0000003I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV0000003O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV0000003O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV0000003I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV0000003I) GetServiceKey() string {
	return "ssv0000003"
}
