//Version: v1.0.0.0
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SV100030I struct {
	QryTpy        string `validate:"required,max=30"` //查询类型  0-介质  1-合约
	MediaType     string `validate:"max=4"`           //介质类型
	MediaNm       string `validate:"max=40"`          //介质号码
	AgreementId   string `validate:"max=30"`          //合约号
	AgreementType string `validate:"max=5"`           //合约类型
	CstmrId       string `validate:"max=20"`          //客户编号
}

type SV100030O struct {
	Records []ContInfo //合约信息
}

// @Desc Build request message
func (o *SV100030I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SV100030I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SV100030O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SV100030O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SV100030I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SV100030I) GetServiceKey() string {
	return "sv100030"
}
