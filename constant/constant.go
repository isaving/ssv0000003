//Version: v0.01
package constant

//define error code
const (
	SV100030 = "SV100030"

	DLS_TYPE_CMM        = "CMM"
	DLS_TYPE_ELEMENT_ID = "common"
	DLS_TYPE_CUS        = "CUS"
)

const (
	ERRCODE01 = "SV12000001"
	ERRCODE02 = "SV12000002"
	ERRCODE03 = "SV12000003"
	ERRCODE04 = "SV99000020"
)
