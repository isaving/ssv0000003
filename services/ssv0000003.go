//Version: v0.01
package services

import (
	"git.forms.io/isaving/sv/ssv0000003/constant"
	"git.forms.io/isaving/sv/ssv0000003/models"
	constant2 "git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	"git.forms.io/universe/dts/common/log"
)

type Ssv0000003Impl struct {
	services.CommonService
	Sv100030O *models.SV100030O
	Sv000003O *models.SSV0000003O
	Sv000003I *models.SSV0000003I
}

// @Desc Ssv0000003 process
// @Author
// @Date 2020-12-04
func (impl *Ssv0000003Impl) Ssv0000003(ssv0000003I *models.SSV0000003I) (ssv0000003O *models.SSV0000003O, err error) {

	impl.Sv000003I = ssv0000003I
	log.Debug("Business serial number,call sv100030 before:", impl.SrcAppProps[constant2.SRCBIZSEQNO])
	for key, value := range impl.SrcAppProps {
		log.Debug(key, ":", value)
	}

	if err := impl.QueryContInfo(); err != nil {
		return nil, err
	}
	log.Debug("Business serial number,call sv100030 after:", impl.SrcAppProps[constant2.SRCBIZSEQNO])
	for key, value := range impl.SrcAppProps {
		log.Debug(key, ":", value)
	}

	if impl.Sv100030O == nil {
		return nil, errors.New("Records not found", constant2.DASNOTFOUND)
	}

	if len(impl.Sv100030O.Records) == 0 {
		return nil, errors.New("Records not found", constant2.DASNOTFOUND)
	}

	ssv0000003O = &models.SSV0000003O{
		Records: impl.Sv100030O.Records,
	}

	return ssv0000003O, nil
}

func (impl *Ssv0000003Impl) QueryContInfo() error {
	sv100030I := models.SV100030I{
		QryTpy:        impl.Sv000003I.QryTpy,
		MediaType:     impl.Sv000003I.MediaType,
		MediaNm:       impl.Sv000003I.MediaNm,
		AgreementId:   impl.Sv000003I.AgreementId,
		AgreementType: impl.Sv000003I.AgreementType,
		CstmrId:       impl.Sv000003I.CstmrId,
	}

	//pack request
	reqBody, err := sv100030I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_TYPE_ELEMENT_ID, constant.SV100030, reqBody)
	if err != nil {
		return err
	}

	sv100030O := models.SV100030O{}
	err = sv100030O.UnPackResponse(resBody)
	if err != nil {
		return err
	}
	impl.Sv100030O = &sv100030O

	return nil
}
