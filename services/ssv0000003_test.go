//Version: v0.01
package services

import (
	"git.forms.io/isaving/sv/ssv0000003/models"
	"testing"
)

var SV100030 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
        "Records": [
            {
                "AgreementID": "1",
                "AgreementType": "1",
                "Currency": "cny",
                "CashtranFlag": "1",
                "AgreementStatus": "1",
                "AccOpnDt": "1",
                "CstmrCntctPh": "1",
                "AccAcount":"1"
            }
        ]
    }
}`

func (this *Ssv0000003Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	switch serviceKey {
	case "SV100030":
		responseData = []byte(SV100030)
	}

	return responseData, nil
}

func TestTryCommService(t *testing.T) {

	Ssv0000003Impl := new(Ssv0000003Impl)

	//传空参
	_, _ = Ssv0000003Impl.Ssv0000003(&models.SSV0000003I{})

	_, _ = Ssv0000003Impl.Ssv0000003(&models.SSV0000003I{
		QryTpy:        "1",
		MediaType:     "1",
		MediaNm:       "1",
		AgreementId:   "1",
		AgreementType: "1",
		CstmrId:       "1",
	})

}
