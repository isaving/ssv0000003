////Version: v0.01
package routers

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/isaving/sv/ssv0000003/controllers"
	"git.forms.io/universe/common/event_handler/register"
	"github.com/astaxie/beego"
)

// @Desc Select the appropriate transaction route according to TOPIC
// @Author
// @Date 2020-12-04
func InitRouter() error {

	eventRouteReg := register.NewEventHandlerRegister()

	bc := beego.AppConfig

	eventRouteReg.Router(bc.String(constant.TopicPrefix + "ssv0000003"),
		&controllers.Ssv0000003Controller{}, "Ssv0000003")

	return nil
}

// @Desc transaction router for swagger
// @Author
// @Date 2020-12-04
func init() {

	ns := beego.NewNamespace("/",
		beego.NSNamespace("/sv",
			beego.NSInclude(
				&controllers.Ssv0000003Controller{},
			),
		),
	)
	beego.AddNamespace(ns)
}
